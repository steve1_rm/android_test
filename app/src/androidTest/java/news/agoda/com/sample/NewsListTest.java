package news.agoda.com.sample;

import android.os.SystemClock;
import android.support.test.espresso.Espresso;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.RecyclerView;
import android.test.suitebuilder.annotation.LargeTest;
import android.widget.Button;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import news.agoda.com.sample.newsList.MainActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by steve on 3/25/16.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class NewsListTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void selectNewsListItem(){
        final String TEST_TITLE = "New School Technology for Class and the Quad";

        /* Scroll to the position */
        onView(withId(R.id.rvNewsListItems)).perform(RecyclerViewActions.scrollToPosition(13));
        SystemClock.sleep(2000);

        /* Click the position */
        onView(withId(R.id.rvNewsListItems)).perform(RecyclerViewActions.actionOnItemAtPosition(13, click()));

        /* Check that the news item that was checked as the correct title */
        SystemClock.sleep(2000);
        onView(withId(R.id.tvTitle)).check(matches(withText(TEST_TITLE)));

        /* Check the story to open the online news article */
        onView(withId(R.id.btnFullStoryLink)).perform(click());
    }
}
