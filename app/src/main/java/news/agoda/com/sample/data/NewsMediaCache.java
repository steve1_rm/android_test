package news.agoda.com.sample.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by steve on 3/23/16.
 */
public class NewsMediaCache {

    private List<NewsEntity> mNewsEntityList = Collections.emptyList();

    private NewsMediaCache() {
        mNewsEntityList = new ArrayList<>();
    }

    /**
     * Singleton design pattern always return the same instance of the cache of news feed
     */
    private static volatile NewsMediaCache mNewsMediaCache;

    public static NewsMediaCache getInstance() {
        if(mNewsMediaCache == null) {
            synchronized(NewsMediaCache.class) {
                if(mNewsMediaCache == null) {
                    mNewsMediaCache = new NewsMediaCache();
                }
            }
        }

        return mNewsMediaCache;
    }

    /**
     * Clear all news feed from the list
     */
    public void clearNewsList() {
        if(!mNewsEntityList.isEmpty()) {
            mNewsEntityList.clear();
        }
    }

    /**
     * Add a news item to the cache
     * @param newsEntity to add to the list of NewsEntity objects
     */
    public void addNewsItem(NewsEntity newsEntity) {
        mNewsEntityList.add(newsEntity);
    }

    /**
     * Return all the news items in the cache
     * @return list of news items
     */
    public List<NewsEntity> getAllNewsItems() {
        return mNewsEntityList;
    }

    /**
     * Return a NewsEntity object from the position
     * @param position of news item
     * @return object or null for invalid position
     */
    public NewsEntity getNewsEntity(int position) {
        if(position > -1) {
            return mNewsEntityList.get(position);
        }

        return null;
    }
}
