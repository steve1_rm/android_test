package news.agoda.com.sample.data;

/**
 * This class represents a media item
 */
public class MediaEntity {
    private String mUrl;
    private String mFormat;
    private int mHeight;
    private int mWidth;
    private String mType;
    private String mSubType;
    private String mCaption;
    private String mCopyright;

    public MediaEntity(String url,
                       String format,
                       int height,
                       int width,
                       String type,
                       String subType,
                       String caption,
                       String copyright) {
        mUrl = url;
        mFormat = format;
        mHeight = height;
        mWidth = width;
        mType = type;
        mSubType = subType;
        mCaption = caption;
        mCopyright = copyright;
    }

    public String getUrl() {
        return mUrl;
    }

    public String getFormat() {
        return mFormat;
    }

    public int getHeight() {
        return mHeight;
    }

    public int getWidth() {
        return mWidth;
    }

    public String getType() {
        return mType;
    }

    public String getSubType() {
        return mSubType;
    }

    public String getCaption() {
        return mCaption;
    }

    public String getCopyright() {
        return mCopyright;
    }

}
