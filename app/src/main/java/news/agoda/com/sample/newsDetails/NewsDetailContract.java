package news.agoda.com.sample.newsDetails;

/**
 * Created by steve on 3/27/16.
 */
public interface NewsDetailContract {
    /**
     * Called if story cannot be opened
     */
    void failedToOpenStory();

    /**
     * Called if story can be opened
     * @param onlineArticleUrl to the story online to be displayed in a browser
     */
    void successToOpenStory(final String onlineArticleUrl);

    /**
     * Fill the strings from the cache to be displayed on the UI
     * @param title of the story
     * @param summaryContent of the story
     * @param picUrl of where to fetch the picture
     */
    void displayNewsDetail(final String title, final String summaryContent, final String picUrl);
}
