package news.agoda.com.sample.newsList;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.DraweeView;
import com.facebook.imagepipeline.request.ImageRequest;

import java.util.Collections;
import java.util.List;

import news.agoda.com.sample.R;
import news.agoda.com.sample.data.NewsEntity;

/**
 * Created by steve on 3/23/16.
 */
public class NewsListAdapter extends RecyclerView.Adapter<NewsListAdapter.NewsListViewHolder> {
    private LayoutInflater mLayoutInflater;
    private List<NewsEntity> mNewsEntityList = Collections.emptyList();

    /**
     * Interface for changing to the detailed news item
     */
    public interface NewsItemSelectedListener {
        void onNewsItemSelected(int newsListPosition);
    }
    private NewsItemSelectedListener mNewsItemSelectedListener;

    public NewsListAdapter(Context context, List<NewsEntity> newsEntityList) {
        mLayoutInflater = LayoutInflater.from(context);
        mNewsEntityList = newsEntityList;
        mNewsItemSelectedListener = (NewsItemSelectedListener)context;
    }

    @Override
    public int getItemCount() {
        return mNewsEntityList.size();
    }

    @Override
    public NewsListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = mLayoutInflater.inflate(R.layout.list_item_news, parent, false);
        return new NewsListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NewsListViewHolder holder, int position) {
        holder.mNewsTitle.setText(mNewsEntityList.get(position).getTitle());

        String thumbnailURL = mNewsEntityList.get(position).getMediaEntity().getUrl();
        DraweeController draweeController =
                Fresco.newDraweeControllerBuilder().setImageRequest(ImageRequest.fromUri(Uri.parse(thumbnailURL))).setOldController(holder.mNewsImage.getController()).build();
        holder.mNewsImage.setController(draweeController);
      }

    class NewsListViewHolder extends RecyclerView.ViewHolder {
        private TextView mNewsTitle;
        private DraweeView mNewsImage;

        public NewsListViewHolder(View itemView) {
            super(itemView);
            mNewsTitle = (TextView)itemView.findViewById(R.id.news_title);
            mNewsImage = (DraweeView)itemView.findViewById(R.id.news_item_image);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /* Return the position for the currently selected news item */
                    int newsListPosition = getAdapterPosition();
                    mNewsItemSelectedListener.onNewsItemSelected(newsListPosition);
                }
            });
        }
    }
}
