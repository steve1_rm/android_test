package news.agoda.com.sample.newsDetails;


import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.request.ImageRequest;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import news.agoda.com.sample.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewsDetailFragment
        extends Fragment
        implements NewsDetailContract {

    public static final String NEWSLISTPOSITION_KEY = "newslistposition";

    private static final String TAG = NewsDetailFragment.class.getSimpleName();
    private NewsDetailPresenter mNewsDetailPresenter;

    @Bind(R.id.tvTitle) TextView mTvTitle;
    @Bind(R.id.tvSummaryContent) TextView mTvSummaryContent;
    @Bind(R.id.dvNewsImage) SimpleDraweeView mDvNewsImage;

    public NewsDetailFragment() {
        // Required empty public constructor
    }

    /**
     * Factory method for receiving the newslist item that will be displayed
     * @param newsListPosition position of news item in cache
     * @return newely created instance of this fragment
     */
    public static NewsDetailFragment getNewInstance(int newsListPosition) {
        final Bundle bundle = new Bundle();
        bundle.putInt(NEWSLISTPOSITION_KEY, newsListPosition);

        NewsDetailFragment newsDetailFragment = new NewsDetailFragment();
        newsDetailFragment.setArguments(bundle);

        return newsDetailFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_detail_view, container, false);

        ButterKnife.bind(NewsDetailFragment.this, view);

        mNewsDetailPresenter = new NewsDetailPresenter(NewsDetailFragment.this, getArguments());
        mNewsDetailPresenter.updateNewsDetail();

        return view;
    }

    @Override
    public void displayNewsDetail(final String title, final String summary, final String picUrl) {
        mTvTitle.setText(title);
        mTvSummaryContent.setText(summary);

        DraweeController draweeController =
                Fresco.newDraweeControllerBuilder()
                        .setImageRequest(ImageRequest.fromUri(Uri.parse(picUrl)))
                        .setOldController(mDvNewsImage.getController()).build();
        mDvNewsImage.setController(draweeController);
    }

    @Override
    public void failedToOpenStory() {
        Toast.makeText(getActivity(), "No valid uri to story", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void successToOpenStory(final String storyUrl) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(storyUrl));
        startActivity(intent);
    }

    @SuppressWarnings("unused")
    @OnClick(R.id.btnFullStoryLink)
    public void openNewsStory() {
        Log.d(TAG, "openNewsStory");
        mNewsDetailPresenter.gotoOnlineNewsStory();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        ButterKnife.unbind(NewsDetailFragment.this);
    }
}
