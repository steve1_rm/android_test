package news.agoda.com.sample.data;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.Writer;

import news.agoda.com.sample.R;

/**
 * Created by steve on 3/26/16.
 */
public class ParseNewsFeed {
    private static final String TAG = ParseNewsFeed.class.getSimpleName();

    /**
     * Get the json from the local resource file and add to the cache to save loading each time
     * @param context used for opening the resource to the raw directory
     * @return the json in string representation
     */
    public boolean getJsonFromResource(Context context) {
        final String jsonString = loadNewsFeed(context);

        if(!jsonString.isEmpty()) {
            if(getJsonResults(jsonString)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Opens and reads from the news_list and writes to a buffer
     * @param context used for opening the resource to the raw directory
     * @return return the json representation as a string or a empty string for failure
     */
    private String loadNewsFeed(Context context) {
        InputStream inputStream = context.getResources().openRawResource(R.raw.news_list);
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];

        try {
            InputStreamReader inputReader = new InputStreamReader(inputStream, "UTF-8");
            BufferedReader bufferReader = new BufferedReader(inputReader);
            int n;
            while ((n = bufferReader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }

            inputStream.close();
        }
        catch (IOException ioException) {
            return "";
        }

        return writer.toString();
    }

    /**
     * Parse each element in the json and create new objects of newsEntity and mediaEntity to fill
     * @param jsonString that will be parsed
     * @return returns true for success or false for failure to parse the json
     */
    private boolean getJsonResults(String jsonString) {
        NewsMediaCache newsMediaCache = NewsMediaCache.getInstance();
        NewsEntity newsEntity;
        MediaEntity mediaEntity = null;
        JSONObject jsonObject;

        /* Clear cache of any news */
        newsMediaCache.clearNewsList();

        try {
            jsonObject = new JSONObject(jsonString);
            JSONArray resultArray = jsonObject.getJSONArray("results");

            for (int i = 0; i < resultArray.length(); i++) {
                String title = "NA";
                String summary = "NA";
                String articleUrl = "NA";
                String byline = "NA";
                String publishedDate = "NA";

                jsonObject = resultArray.getJSONObject(i);

                /* Sanity check for each json attribute */
                if(jsonObject.has("title")) {
                    title = jsonObject.getString("title");
                }

                if(jsonObject.has("abstract")) {
                    summary = jsonObject.getString("abstract");
                }

                if(jsonObject.has("url")) {
                    articleUrl = jsonObject.getString("url");
                }

                if(jsonObject.has("byline")) {
                    byline = jsonObject.getString("byline");
                }

                if(jsonObject.has("published_date")) {
                    publishedDate = jsonObject.getString("published_date");
                }

                /* Get the json array object "multimedia" */
                if(jsonObject.has("multimedia")) {
                    JSONArray mediaArray = jsonObject.optJSONArray("multimedia");

                    if(mediaArray != null) {
                        for(int k = 0; k < mediaArray.length(); k++) {
                            String url = "NA";
                            String format = "NA";
                            int height = 0;
                            int width = 0;
                            String type = "NA";
                            String subType = "NA";
                            String caption = "NA";
                            String copyRight = "NA";

                            JSONObject mediaObject = mediaArray.getJSONObject(k);

                            if(mediaObject.has("url")) {
                                url = mediaObject.getString("url");
                            }

                            if(mediaObject.has("format")) {
                                format = mediaObject.getString("format");
                            }

                            if(mediaObject.has("height")) {
                                height = mediaObject.getInt("height");
                            }

                            if(mediaObject.has("width")) {
                                width = mediaObject.getInt("width");
                            }

                            if(mediaObject.has("type")) {
                                type = mediaObject.getString("type");
                            }

                            if(mediaObject.has("subtype")) {
                                subType = mediaObject.getString("subtype");
                            }

                            if(mediaObject.has("caption")) {
                                caption = mediaObject.getString("caption");
                            }

                            if(mediaObject.has("copyright")) {
                                copyRight = mediaObject.getString("copyright");
                            }

                            mediaEntity = new MediaEntity(url, format, height, width, type, subType, caption, copyRight);
                        }

                        newsEntity = new NewsEntity(title, summary, articleUrl, byline, publishedDate, mediaEntity);
                        newsMediaCache.addNewsItem(newsEntity);
                    }
                }
            }
        }
        catch (JSONException e) {
            Log.e(TAG, "fail to parse json string: " + e.getMessage());
            return false;
        }

        Log.d(TAG, "All json results successfully loaded");
        return true;
    }

}
