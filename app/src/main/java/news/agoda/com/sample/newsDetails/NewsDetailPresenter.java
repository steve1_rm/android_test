package news.agoda.com.sample.newsDetails;

import android.os.Bundle;
import android.webkit.URLUtil;

import news.agoda.com.sample.data.NewsEntity;
import news.agoda.com.sample.data.NewsMediaCache;

/**
 * Created by steve on 3/27/16.
 */
public class NewsDetailPresenter {
    private NewsDetailContract mNewsDetailContract;
    private NewsEntity mNewsEntity;

    public NewsDetailPresenter(NewsDetailContract view, final Bundle bundle) {
        mNewsDetailContract = view;
        mNewsEntity = getNewEntity(bundle);
    }

    /**
     * Gets the online article to be displayed in a browser
     */
    public void gotoOnlineNewsStory() {
        final String onlineArticleUrl = mNewsEntity.getArticleUrl();

        if(mNewsEntity != null) {
            if (URLUtil.isValidUrl(onlineArticleUrl)) {
                mNewsDetailContract.successToOpenStory(onlineArticleUrl);
            }
            else {
                mNewsDetailContract.failedToOpenStory();
            }
        }
    }

    /**
     * Gets the detail of the story to be updated in the UI
     */
    public void updateNewsDetail() {
        if(mNewsEntity != null) {
            final String title = mNewsEntity.getTitle();
            final String summaryContent = mNewsEntity.getSummary();
            final String picUrl = mNewsEntity.getMediaEntity().getUrl();

            mNewsDetailContract.displayNewsDetail(title, summaryContent, picUrl);
        }
    }

    /**
     * Check that we have something to display
     * @param bundle containing the position of the news detail we want to display
     * @return NewsEntity object or null
     */
    private NewsEntity getNewEntity(Bundle bundle) {
        if (bundle != null) {
            if (!bundle.isEmpty()) {
                final int position = bundle.getInt(NewsDetailFragment.NEWSLISTPOSITION_KEY, -1);
                if (position > -1) {
                    return NewsMediaCache.getInstance().getNewsEntity(position);
                }
            }
        }

        return null;
    }
}
