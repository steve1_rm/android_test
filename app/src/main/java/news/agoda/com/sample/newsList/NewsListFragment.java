package news.agoda.com.sample.newsList;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.facebook.drawee.backends.pipeline.Fresco;

import java.util.List;

import news.agoda.com.sample.R;
import news.agoda.com.sample.data.NewsEntity;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewsListFragment extends Fragment implements NewsListContract {
    private static final String TAG = NewsListFragment.class.getSimpleName();
    private RecyclerView mRecyclerView;

    public NewsListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_news_list, container, false);

        /* Has to be called once to initialize view */
        Fresco.initialize(getActivity());

        mRecyclerView = (RecyclerView)view.findViewById(R.id.rvNewsListItems);

        /* Get the news and fill the news new cache */
        final NewsListPresenter newsListPresenter = new NewsListPresenter(NewsListFragment.this, getActivity());
        newsListPresenter.retrieveNewsFeed();

        return view;
    }

    @Override
    public void failedToGetNews() {
        Toast.makeText(getActivity(), "Failed to get news", Toast.LENGTH_SHORT).show();
        Log.e(TAG, "Failed to get the news");
    }

    @Override
    public void successToGetNews(List<NewsEntity> newsEntityList) {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        NewsListAdapter adapter = new NewsListAdapter(getActivity(), newsEntityList);
        mRecyclerView.setAdapter(adapter);
    }

}
