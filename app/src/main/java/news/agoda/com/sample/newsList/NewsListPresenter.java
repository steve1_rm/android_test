package news.agoda.com.sample.newsList;

import android.content.Context;
import android.util.Log;

import news.agoda.com.sample.data.NewsMediaCache;
import news.agoda.com.sample.data.ParseNewsFeed;

/**
 * Created by steve on 3/23/16.
 */
public class NewsListPresenter {
    private static final String TAG = NewsListPresenter.class.getSimpleName();

    private NewsListContract mView;
    private Context mContext;

    public NewsListPresenter(NewsListContract view, Context context) {
        mView = view;
        mContext = context;
    }

    /**
     * Gets the news feed to be display in the list of news items
     */
    public void retrieveNewsFeed() {
        if(new ParseNewsFeed().getJsonFromResource(mContext)) {
            if(!NewsMediaCache.getInstance().getAllNewsItems().isEmpty()) {
                mView.successToGetNews(NewsMediaCache.getInstance().getAllNewsItems());
            }
        }
        else {
            mView.failedToGetNews();
            Log.e(TAG, "failed to get json from resource");
        }
    }
}
