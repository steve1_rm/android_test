package news.agoda.com.sample.newsList;

import java.util.List;

import news.agoda.com.sample.data.NewsEntity;

/**
 * Created by steve on 3/23/16.
 */
public interface NewsListContract {
    /**
     * Called in the json cannot be retrieved or failed to parse
     */
    void failedToGetNews();

    /**
     * Json has been retrieved to be displayed
     * @param newsEntityList to be displayed in the recrycler view
     */
    void successToGetNews(List<NewsEntity> newsEntityList);
}
