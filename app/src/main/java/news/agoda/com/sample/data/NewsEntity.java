package news.agoda.com.sample.data;

/**
 * This represents a news item
 */
public class NewsEntity {
    private String mTitle;
    private String mSummary;
    private String mArticleUrl;
    private String mByline;
    private String mPublishedDate;
    private MediaEntity mMediaEntity;

    public NewsEntity(String title,
                      String summary,
                      String articleUrl,
                      String byline,
                      String publishedDate,
                      MediaEntity mediaEntity) {

        mTitle = title;
        mSummary = summary;
        mArticleUrl = articleUrl;
        mByline = byline;
        mPublishedDate = publishedDate;

        /* For each Newslist add the multimedia item */
        mMediaEntity = mediaEntity;
    }

    public MediaEntity getMediaEntity() {
        return mMediaEntity;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getSummary() {
        return mSummary;
    }

    public String getArticleUrl() {
        return mArticleUrl;
    }

    public String getByline() {
        return mByline;
    }

    public String getPublishedDate() {
        return mPublishedDate;
    }
}
